import Koa       from "koa";
import Router    from "koa-router";
import {pageCsv} from "./csv";

const app = new Koa();

let router = new Router();

router.get("/api/csv", pageCsv);

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);