import EventEmitter from "events";

class Emitter extends EventEmitter {
}

const myEmitter = new Emitter();

export {myEmitter};
