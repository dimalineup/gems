import {diamonds, ritani_items as ritani, test} from "./models";
import {WEIGHTS}                                from "../config";
import {myEmitter}                              from "../events";

async function init() {
    console.log("INIT");
    diamonds.deleteMany({}, err => {
        if (err)
            console.error(err);
    });
    WEIGHTS.forEach(async weight => {
        let arr = await  test.find({size: {$gte: weight.a, $lte: weight.b}}).limit(1000).exec();
        arr.forEach(async doc => {

            let select = await ritani.find({
                color: doc.color,
                clarity: doc.clarity,
                shape: doc.shape_short,
                symmetry: doc.symmetry_short,
                polish: doc.polish_short,
                weight: {$gte: weight.a, $lte: weight.b}
            }).exec();
            if (select.length) {
                let d = await new diamonds({
                    stock_num: doc.stock_num,
                    cert_url: doc.cert_url,
                    shape_short: doc.shape_short,
                    size: doc.size,
                    color: doc.color,
                    clarity: doc.clarity,
                    cut_short: doc.cut_short,
                    polish: doc.polish,
                    symmetry_short: doc.symmetry_short,
                    fluorescence_intensity_short: doc.fluorescence_intensity_short,
                    lab: doc.lab,
                    depth_percent: doc.depth_percent,
                    meas_depth: doc.meas_depth,
                    meas_length: doc.meas_length,
                    meas_width: doc.meas_width,
                    table_percent: doc.table_percent,
                    total_sales_price: doc.total_sales_price,
                    vendor_name: doc.vendor_name,
                    select
                });
                await d.save();
            }
        });
    });
}

let COUNT_CONNECT = 0;
myEmitter.on("start", async () => {
    COUNT_CONNECT++;
    if (COUNT_CONNECT === 2) {
        await init();

        /* setInterval(async () => {
         init();
         }, 86400000000);*/
    }
});