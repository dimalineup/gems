import mongoose    from "mongoose";
import {myEmitter} from "../../../events";

const Schema = mongoose.Schema;

let db = mongoose.createConnection("mongodb://heroku_xkm4v3dp:hi8q5m7voddpoln8hhpggt6gap@ds251362.mlab.com:51362/heroku_xkm4v3dp", {useNewUrlParser: true});
db.once("open", () => {
    console.log("Connect open server ...");
    myEmitter.emit("start");
});

const ritaniSchema = new Schema(
    {
        itemId: String,
        shape: String,
        weight: String,
        cut: String,
        clarity: String,
        color: String,
        polish: String,
        symmetry: String,
        fluorescence: String,
        table: String,
        depth: String,
        culet: String,
        price: String,
        scrapedAt: Date,
        raw: Object
    }, {
        collection: "ritani_items"
    });
const ritani_items = db.model("ritani_items", ritaniSchema);

export {ritani_items, ritaniSchema};
