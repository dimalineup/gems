import mongoose       from "mongoose";
import {ritaniSchema} from "../ritani";
import {myEmitter}    from "../../../events";

const Schema = mongoose.Schema;
let db = mongoose.createConnection("mongodb://heroku_xkm4v3dp:hi8q5m7voddpoln8hhpggt6gap@ds251362.mlab.com:51362/heroku_xkm4v3dp", {useNewUrlParser: true});
db.once("open", () => {
    console.log("Connect open home ...");
    myEmitter.emit("start");
});
const testSchema = new Schema({
    id: String,
    availability_change_date: String,
    available: String,
    caratCluster: String,
    cert_comment: String,
    cert_num: String,
    cert_source: String,
    cert_source_url: String,
    cert_url: String,
    clarity: String,
    clarity_long: String,
    clarity_short: String,
    color: String,
    comments: String,
    created_at: String,
    crown_angle: String,
    crown_height: String,
    culet_size: String,
    culet_size_long: String,
    culet_size_short: String,
    cut: String,
    cut_long: String,
    cut_short: String,
    depth_percent: String,
    description: String,
    discount_percent: String,
    enrichedAt: String,
    fancy_color_intensity_long: String,
    fancy_color_intensity_short: String,
    fancy_color_long: String,
    fancy_color_short: String,
    fluorescence_intensity_long: String,
    fluorescence_intensity_short: String,
    girdle_condition: String,
    girdle_max: String,
    girdle_max_long: String,
    girdle_max_short: String,
    girdle_min: String,
    girdle_min_long: String,
    girdle_min_short: String,
    girdle_percent: String,
    image_thumb_url: String,
    image_url: String,
    is_fancy: String,
    is_mounted: String,
    is_separable: String,
    jewelry_style: String,
    lab: String,
    lab_long: String,
    lab_short: String,
    laser_inscription: String,
    master_diamond_id: String,
    meas_depth: String,
    meas_length: String,
    meas_width: String,
    milky: String,
    mount_price: String,
    pavilion_angle: String,
    pavilion_depth: String,
    polish: String,
    polish_long: String,
    polish_short: String,
    priceCluster: String,
    price_per_carat: String,
    sell_price: String,
    shape: String,
    shape_long: String,
    shape_short: String,
    size: String,
    status: String,
    stock_num: String,
    symmetry: String,
    symmetry_long: String,
    symmetry_short: String,
    table_percent: String,
    total_sales_price: String,
    treatment: String,
    updated_at: String,
    vendor_accepts_memo: String,
    vendor_city: String,
    vendor_country: String,
    vendor_email: String,
    vendor_id: String,
    vendor_iphone: String,
    vendor_mobile_phone: String,
    vendor_name: String,
    vendor_phone: String,
    vendor_state: String,
    vendor_street_address: String,
    vendor_zip_code: String,
    video_url: String,
    views: String
}, {collection: "test"});
const test = db.model("test", testSchema);

const diamondsSchema =
    new Schema({
        stock_num: String,
        cert_url: String,
        shape_short: String,
        size: String,
        color: String,
        clarity: String,
        cut_short: String,
        polish: String,
        symmetry_short: String,
        fluorescence_intensity_short: String,
        lab: String,
        depth_percent: String,
        meas_depth: String,
        meas_length: String,
        meas_width: String,
        table_percent: String,
        total_sales_price: String,
        vendor_name: String,
        select: [ritaniSchema]
    })
;
const diamonds = db.model("myDiamonds", diamondsSchema);

export {test, diamonds};