import fs          from "fs";
import csv         from "fast-csv";
import path        from "path";
import {diamonds}  from "../db/models";
import {FILE_NAME} from "../config";

let csvStream = csv.createWriteStream({headers: true}).transform(doc => {
    return {
        stock_num: doc.stock_num,
        cert_url: doc.cert_url,
        shape_short: doc.shape_short,
        size: doc.size,
        color: doc.color,
        clarity: doc.clarity,
        cut_short: doc.cut_short,
        polish: doc.polish,
        symmetry_short: doc.symmetry_short,
        fluorescence_intensity_short: doc.fluorescence_intensity_short,
        lab: doc.lab,
        depth_percent: doc.depth_percent,
        meas_depth: doc.meas_depth,
        meas_length: doc.meas_length,
        meas_width: doc.meas_width,
        table_percent: doc.table_percent,
        total_sales_price: doc.total_sales_price,
        vendor_name: doc.vendor_name,
        select__itemId: doc.select__itemId,
        select__price: doc.select__price,
        select__shape: doc.select__shape,
        select__weight: doc.select__weight,
        select__color: doc.select__color,
        select__clarity: doc.select__clarity,
        select__cut: doc.select__cut,
        select__polish: doc.select__polish,
        select__symmetry: doc.select__symmetry,
        select__fluorescence: doc.select__fluorescence
    };
});
let writableStream = fs.createWriteStream(FILE_NAME);

writableStream.on("finish", () => {
    console.log("File done...");
});

csvStream.pipe(writableStream);

let pushData = async data => {
    data.forEach(async d => {
        await d.select.forEach(async (s, i) => {
            let a;
            if (i === 0) {
                a = {
                    stock_num: d.stock_num,
                    cert_url: d.cert_url,
                    shape_short: d.shape_short,
                    size: d.size,
                    color: d.color,
                    clarity: d.clarity,
                    cut_short: d.cut_short,
                    polish: d.polish,
                    symmetry_short: d.symmetry_short,
                    fluorescence_intensity_short: d.fluorescence_intensity_short,
                    lab: d.lab,
                    depth_percent: d.depth_percent,
                    meas_depth: d.meas_depth,
                    meas_length: d.meas_length,
                    meas_width: d.meas_width,
                    table_percent: d.table_percent,
                    total_sales_price: d.total_sales_price,
                    vendor_name: d.vendor_name,
                    select__itemId: s.raw.id,
                    select__price: s.price,
                    select__shape: s.shape,
                    select__weight: s.weight,
                    select__color: s.color,
                    select__clarity: s.clarity,
                    select__cut: s.cut,
                    select__polish: s.polish,
                    select__symmetry: s.symmetry,
                    select__fluorescence: s.fluorescence
                };
            } else {
                a = {
                    stock_num: "",
                    cert_url: "",
                    shape_short: "",
                    size: "",
                    color: "",
                    clarity: "",
                    cut_short: "",
                    polish: "",
                    symmetry_short: "",
                    fluorescence_intensity_short: "",
                    lab: "",
                    depth_percent: "",
                    meas_depth: "",
                    meas_length: "",
                    meas_width: "",
                    table_percent: "",
                    total_sales_price: "",
                    vendor_name: "",
                    select__itemId: s.raw.id,
                    select__price: s.price,
                    select__shape: s.shape,
                    select__weight: s.weight,
                    select__color: s.color,
                    select__clarity: s.clarity,
                    select__cut: s.cut,
                    select__polish: s.polish,
                    select__symmetry: s.symmetry,
                    select__fluorescence: s.fluorescence
                };
            }
            await  csvStream.write(a);
        });
    });
};
let pageCsv = async ctx => {
    let d = await diamonds.find({}).limit(1000).exec();
    if (d.length) {
        await pushData(d);
    }
    ctx.attachment(FILE_NAME);
    ctx.set("Content-disposition", "attachment; filename=" + FILE_NAME);
    ctx.body = fs.createReadStream(path.join(__dirname, "..", "", FILE_NAME));
};
export {pageCsv};



